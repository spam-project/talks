\documentclass[usepdftitle=false, 10pt, aspectratio=169]{beamer} % handout
\usetheme[
    progressbar=none, % progressbar = none / foot / frametitle / head
    numbering=counter,  % none, counter, fraction
    % sectionpage=none,
    % titleformat=smallcaps
]{metropolis}
\setbeamertemplate{section in toc}[sections numbered]

\input{include/packages}
\input{include/preferences}
\input{include/shortcuts}

\title{\includegraphics[height=2em,valign=c]{images/spam-workshop.png}}
\subtitle{{\bf Global DVC}\\What is it all about?}
\author{
    \begin{columns}
        \begin{column}{0.4\linewidth}
            Emmanuel Roubin \\
            2024/06/13
        \end{column}
        \begin{column}{0.4\linewidth}
            \hfill Laboratoire 3SR\\ \hfill Université Grenoble Alpes (France)
        \end{column}
    \end{columns}
}
\date{}
\institute{
  \begin{columns}
    \begin{column}{0.7\linewidth}
      ~
      % \normalsize Laboratoire 3SR\\ Université Grenoble Alpes (France)
    \end{column}
    \begin{column}{0.3\linewidth}
      \vspace{-4em}
      \hfill\includegraphics[height=3em,valign=c]{images/logo-3sr.png} \hspace{1em} \includegraphics[height=3em,valign=c]{images/logo-uga.png}
      \hfill
    \end{column}
  \end{columns}
  \hspace*{-\beamerleftmargin}%
  \includegraphics[width=\paperwidth]{images/montagne-flip.png}
  \vspace{-9.5em}
}


\begin{document}
\addtocounter{framenumber}{1}

\maketitle

\begin{frame}
    \begin{description}
        \item[\color{myblue} Little recap] on local DVC
        \item[\color{myblue} Basic principles] of global DVC
        \item[\color{myblue} Going further] with the mechanical regularisation
    \end{description}
\end{frame}

\begin{frame}\centering\bf
  Little recap on local DVC
\end{frame}

\begin{frame}{The general idea behind (local) DVC}

  \begin{mybboxt}{\mybold{D}igital \mybold{V}olume \mybold{C}orrelation or DVC}
    Doing a DVC between two images $f(\x)$ and $g(\x)$ means finding a transformation of $g(\x)$, noted $\widetilde g(\x)$ $\F$, that minimizes the ``difference'' between $f(\x)$ and $\widetilde g(\x)$.

    \[
      \phic = \sum_\Omega \| \widetilde g(\x) - f(\x) \|_2
    \]

  \end{mybboxt}
  \par In the local approach we transform the $g$ with a tranformation operator $\F$ such that:
  \[\widetilde g(\x) = g(\F \cdot \x) \quad \text{with} \quad \F = \left(\begin{array}{cccc} {\color{myblue} \boldsymbol  F_{11}} & {\color{myblue} \boldsymbol  F_{12}} & {\color{myblue} \boldsymbol  F_{13}} & {\color{myred} \boldsymbol t_x} \\ {\color{myblue} \boldsymbol  F_{21}} & {\color{myblue} \boldsymbol  F_{22}} & {\color{myblue} \boldsymbol  F_{23}} & {\color{myred} \boldsymbol t_y} \\ {\color{myblue} \boldsymbol  F_{31}} & {\color{myblue} \boldsymbol  F_{32}} & {\color{myblue} \boldsymbol  F_{33}} & {\color{myred} \boldsymbol t_z} \\ 0 & 0 & 0 & 1 \end{array}\right) \ \ \text{and} \ \ \x = \left(\begin{array}{c} x \\ y \\ z \\ 1 \end{array}\right)\]

\end{frame}

\begin{frame}{The general idea behind (local) DVC}
  \par The process can be made efficient by an \mybold{iteratif process} based on a \mybold{taylor expansion} (for small changes of $\F$: $\delta\F$) of the transformation applied to $g$:
  \[ g(\F^{(n+1)}\cdot\x) = \widetilde g^{(n)}(\x) + {\color{myred}\boldsymbol \nabla \widetilde g^{(n)}(\x)} \cdot \delta\F^{(n+1)} \cdot \x^{(n)} \]
  which involves the {\color{myred}\bf gradient} of the image.
  
  \begin{mybbox}
    A Newton scheme can be used to solve the system:
    \[{\boldsymbol M}^{(n)} \delta\F^{(n+1)} = {\boldsymbol A}^{(n)}\]
    where the matrices
    \begin{itemize}
    \item ${\boldsymbol M}$ contains the gradient of $g$ and
    \item ${\boldsymbol A}$ contains the difference between $f$ and $g$
    \end{itemize}
  \end{mybbox}
  \bibperso{Lucas, B. D. and Kanade, T.}{An iterative image registration technique with an application to stereo vision}{1981}
\end{frame}


\begin{frame}{Registration: one point transformation}
    \scalebox{1.0}{\input{tikz/registration_t01}}
\end{frame}

\begin{frame}{Registration: one point transformation}

  % \begin{mygboxt}{Results for the tension test}
  \[\F = \left(\begin{array}{cccc} {\color{myblue}\boldsymbol{1.0001}} & -0.0001 & 0.0002 & {\color{mygreen}\boldsymbol{-332.194}} \\ 0.0001 & {\color{myred}\boldsymbol{0.9998}} & -0.0008 & 0.5356 \\ -0.0002 & 0.0006 & {\color{myred}\boldsymbol{0.9999}} & 6.9019 \\ 0 & 0 & 0 & 1 \end{array}\right)\]

  \vspace{2em}
  \begin{itemize}
  \item Rigid body motion of $\SI{300}{\micro\metre}$ and $0.01^\circ$ $\rightarrow$ useful for later!
  \item Relative displacements of $\SI{5}{\micro\metre}$ (comes from the strain part of $\F$)
  \end{itemize}
  % \end{mygboxt}

  % \begin{myrbox}
  %   \begin{itemize}
  %   \item {\bf Very small strains} in tension ($\varepsilon\approx \SI{0.25}{\micro def}$, close to the noise)
  %   \item Better in compression (as the transformation involved are larger)
  %   \item We still identify the rigid body motion (helpfull for the next step)
  %   \end{itemize}
  % \end{myrbox}
\end{frame}

\begin{frame}{Regular grid: field of transformations}
    \begin{mybboxt}{From single point registration to regular grid correlation}
        \par The same algorithm can be apply on {\bf several {\color{myred}independent} windows} of the image (``imagettes'') forming a regular grid
        \par The result of the correlation is then a {\bf field} of $\F$
        \par The registration is still used as an {\bf initial guess} for each window
    \end{mybboxt}

    \begin{myrbox}
        The size of each window is chosen based on a trade off between:
        \begin{itemize}
            \item spatial resolution of the field (the smaller the better)
            \item noise handeling (the bigger the better)
        \end{itemize}
    \end{myrbox}
\end{frame}

\begin{frame}{Regular grid: field of transformations}
  \scalebox{1.0}{\input{tikz/regulargrid_t01}}
  
  \onslide<5->{
    \vspace{-0.5em}
    \begin{myrbox}
      \centering\bf
      All windows / $\F$ are independant
    \end{myrbox}
  }
\end{frame}

\begin{frame}\centering\bf
  Local vs Global DVC
\end{frame}

\begin{frame}{A Finite Element mesh to transform an image}
  The global DVC problem can be expressed as the following functional to minimize:
  \[
    \phic = \sum_\Omega \| g(\x + {\color{myblue}\u(\x)}) - f(\x) \|_2
    \label{eq:residual}
  \]

  \pause
  which, in it's weak form can be solved with a Newton-Raphson method:
  \[
    {\color{myorange}\Mc}^{(n)} {\color{myblue}\delta\d}^{(n + 1)} = {\color{myred}\boldsymbol{b}}^{(n)}
  \]
  \pause
  where:
  \begin{itemize}
  \item ${\color{myblue}\u(\x)} = \boldsymbol{N}(\x){\color{myblue}\d}$ is the displacement field supported but standards FE shape functions.
  \item ${\color{myblue}\delta\d}$ is the increment of {\color{myblue}displacement} field unknowns ${\color{myblue}\d}$.
  \item ${\color{myorange}\Mc}$ can be seen as a FE assembled stiffness matrix where material properties are replaced by the {\color{myorange}gradient} of the images.
  \item ${\color{myred}\boldsymbol{b}}$ contains the {\color{myred}residual} supported by the shape functions.
  \end{itemize}
\end{frame}


\begin{frame}{Local versus Global $\rightarrow$ $\F$ versus $\boldsymbol{N}$} 
  \begin{mypboxt}{Minimisation problem}
    \[
      \phic = \sum_\Omega \| \widetilde g(\x) - f(\x) \|_2
    \]
  \end{mypboxt}\vspace{-1em}
  \begin{columns}
    \begin{column}[T]{0.5\linewidth}
      \begin{mybboxt}{Local DVC}
        \onslide<2->{
          Transformation of the image
          \[\widetilde g(\x) = g(\F\cdot\x)\]
          Resolution scheme $\forall i \in \{1, \dots, n_\text{win}\}$
          \[{\boldsymbol M_{\Omega_i}} \delta\F_{\Omega_i} = {\boldsymbol A_{\Omega_i}}\]
          Size of the $n_\text{win}$ systems: $12$
        }
      \end{mybboxt}
    \end{column}
    \begin{column}[T]{0.5\linewidth}
      \begin{mygboxt}{Global DVC}
        \onslide<3->{
          Transformation of the image
          \[\widetilde g(\x) = g(\x + \u(\x))\]
          Resolution scheme
          \[\Mc \delta\d = \boldsymbol{b}\]
          Size of the unique system: $3\times n_\text{nodes}$
        }
      \end{mygboxt}
    \end{column}
  \end{columns}
  
\end{frame}


\begin{frame}{Local versus Global $\rightarrow$ $\F$ versus $\boldsymbol{N}$}
  \centering
  \scalebox{1.0}{\input{tikz/local_vs_global}}
\end{frame}

\begin{frame}{Local versus Global $\rightarrow$ $\F$ versus $\boldsymbol{N}$}
  \centering
  \scalebox{1.0}{\input{tikz/local_vs_global_refining}}
\end{frame}

\begin{frame}\centering\bf
  Assembly of the global DVC system
\end{frame}

\begin{frame}{Matrix / vector assembly}
  \begin{adjustwidth}{-10cm}{-10cm}
    \centering
    \scalebox{1.0}{\input{tikz/global_assemble}}
  \end{adjustwidth}
\end{frame}


\begin{frame}\centering\bf
  Going further with the global DVC
\end{frame}

\begin{frame}{Tikhonov regularization}
  We want to minimize the following problem which is considered ill posed (no unique solution)
  \[ \| \boldsymbol A \x -\boldsymbol{b} \|_2^2 \]
  
  \pause
  \par The principle of the Tikhonov regularization is to add a second functional
  \[ \| \boldsymbol A \x -\boldsymbol{b} \|_2^2 + \|\boldsymbol\Gamma \x \|_2^2 \]
  % \[ \Mc \delta \d = \boldsymbol{b} \]
  \begin{itemize}
    \item If $\boldsymbol\Gamma = \alpha \mathbb{I}$ ($L_2$ normalization): gives preferences to smaller norms.
    \item If $\boldsymbol\Gamma = \nabla$: enforce smoothness. 
  \end{itemize}
\end{frame}

\begin{frame}{Mechanical regularization}

    \mybold{Mechanically driven Tikhonov regularization}

    \par \underline{Equilibrium gap method}:
    Minimizing the distance between current solution $\u$ and that which satisfies the \mybold{equilibrium for linear elasticity}.
    \vspace{-1em}
    \begin{columns}
        \begin{column}[]{0.4\linewidth}
            \[\def\arraystretch{1.5}
                \left\{
                \begin{array}{l}
                    \phic(\d) = \sum_\Omega \|g(\x + \u(\x)) - f(\x) \|_2 \hspace{5em}\\
                    \only<1>{\color{myblue}???\\}
                    \only<2->{\color{myblue}\phim(\d) = \|\boldsymbol{K}\u - \boldsymbol{f}\|^2 \quad (\text{Neumann})\\}
                    \only<-2>{\color{myred}???}
                    \only<3->{\color{myred}\varphi_{\mathcal{S}_i}(\d) = \int_{\mathcal{S}_i} \| \boldsymbol{\nabla} (\boldsymbol{\sigma} \cdot \boldsymbol{n}) \|^2 d\x  \quad (\text{Dirichlet})}
                \end{array}\right.
            \]
            The regularised problem becomes:
            \[
                \left(\Mc + {\color{mygreen}\boldsymbol{M}_\text{reg}} \right) \delta\d = \boldsymbol{b} - {\color{mygreen}\boldsymbol{M}_\text{reg}\d} \hfill
            \]
        \end{column}
        \begin{column}[]{0.6\linewidth}
            \onslide<2->{
                \begin{center}
                    \vspace{-1em}
                    \scalebox{0.75}{\input{tikz/global_mendoza}}
                \end{center}
            }
        \end{column}
    \end{columns}
    \vspace{1em}
    \bibperso{Arturo Mendoza, Jan Neggers, François Hild, Stéphane Roux}{Complete mechanical regularization applied to digital image and volume correlation}{2019}
\end{frame}

\begin{frame}{Mechanical regularization}
  Mechanical regularisation boils down to a Tikhonov regularisation with a fancy difference operator. Thus, it \mybold{ensures smoothness of the result} and what we call a \mybold{soft} regularisation. % ({\it i.e.} does not depend on the mesh size).
  
  The \mybold{weight of the functionnal} can be related to a characteristic length $\xi$ that acts as a low-pass filters
  \[ \phic(\d) + \alpha(\xi) \phim(\d) \]

  If the wavelength of the signal is
  \begin{description}
  \item[$> \xi$] then the DVC prevails
  \item[$< \xi$] then the regularisation prevails
  \end{description}

  \vfill
  \bibperso{Arturo Mendoza, Jan Neggers, François Hild, Stéphane Roux}{Complete mechanical regularization applied to digital image and volume correlation}{2019}
\end{frame}

\begin{frame}{Mechanical regularization}

  \begin{figure}
    \includegraphics[height=7cm]{images/addict}
  \end{figure}
  \vspace{-1em}
  \bibperso{Ali Rouwane, Robin Bouclier, Jean-Charles Passieux, Jean-Noël Périé}{Architecture-Driven Digital Image Correlation Technique (ADDICT) \dots}{2022}
\end{frame}

\begin{frame}{Example}
    \begin{figure}
        \subfloat[$f$]       {\includegraphics[height=5cm,   valign=c]{images/V4-01-b1}}\hspace{1em}
        \subfloat[$g$]       {\includegraphics[height=5cm,   valign=c]{images/V4-02-b1}}\hspace{4em}
        \subfloat[Global DVC]{\includegraphics[height=5cm,   valign=c]{images/V4-global-dvc}}\hspace{0.5em}
        \subfloat            {\includegraphics[height=2.5cm, valign=c]{images/V4-global-dvc-scale}}
        \caption*{Some early results: triaxial tests on clay (SPAM benchmark)}
    \end{figure}
\end{frame}


%% CODE EXAMPLE
%% \begin{frame}[fragile]{\includegraphics[height=0.5cm]{images/logo-spam.png}\ \ {\tt SPAM} example: Regular grid algorithm}
%% 
%%     Since regular grid algorithm is more complicated than registration, a {\tt script} is available in {\tt SPAM} than can be called from the {\tt bash}
%% \begin{lstlisting}[language=Bash]
%%     bash> spam-ldic                  # The client
%%         -tsv -tif \                  # Ask for TSV file and TIFF file output
%%         -Ffile V4-registration.tsv \ # Load the registration
%%         -glt 20000 \                 # Do not correlate windows if mean grey level below
%%         -hws 10 \                    # Half-window size of the correlation window
%%         V4-01.tif V4-02.tif          # The two tiff files to load\end{lstlisting}
%% 
%%     A lot more options are available, see the documentation: \href{https://www.spam-project.dev/docs}{https://www.spam-project.dev/docs}
%% 
%% \end{frame}

\end{document}
