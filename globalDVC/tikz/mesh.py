import gmsh
import sys

def write_mesh(lc, name):
    
    gmsh.model.add("square")
    a = 2.5
    p1 = gmsh.model.geo.addPoint(-a, -a, 0, lc)
    p2 = gmsh.model.geo.addPoint(-a, +a, 0, lc)
    p3 = gmsh.model.geo.addPoint(+a, +a, 0, lc)
    p3 = gmsh.model.geo.addPoint(+a, -a, 0, lc)
    l1 = gmsh.model.geo.addLine(1, 2)
    l2 = gmsh.model.geo.addLine(2, 3)
    l3 = gmsh.model.geo.addLine(3, 4)
    l4 = gmsh.model.geo.addLine(4, 1)
    cl = gmsh.model.geo.addCurveLoop([l1, l2, l3, l4])
    gmsh.model.geo.addPlaneSurface([cl])
    gmsh.model.geo.synchronize()
    gmsh.model.mesh.generate(2)

    node_tags, coord, _ = gmsh.model.mesh.getNodesByElementType(2)
    element_tags, node_tags_element = gmsh.model.mesh.getElementsByType(2)

    coordinates = {i: c[:2] for i, c in zip(node_tags, coord.reshape(node_tags.shape[0], 3))}


    with open(f"mesh2D_{name}.tex", "w+") as f:
        for i, c in coordinates.items():
            f.write(f'\\coordinate (node{i}) at ({c[0]} + \\s, {c[1]});\n')

        for p1, p2, p3 in node_tags_element.reshape(element_tags.shape[0], 3):
            f.write(f'\\filldraw [draw=mygreen, fill=mygreen, fill opacity=0.2, line width=1] (node{p1}) -- (node{p2}) -- (node{p3}) -- (node{p1});\n')

        for i, c in coordinates.items():
            f.write(f'\\filldraw [draw=myblue, fill=myblue!80, line width=0.5] (node{i}) circle (2pt);\n')

    with open(f"mesh2D_{name}_matrix.tex", "w+") as f:

        a = -5.0
        b = 0.0
        n = len(list(set(node_tags_element)))
        w = (b - a) / n
        print(n, w)
        
        f.write(f"\def\w{{{w:.4f}}}\n")
        for p1, p2, p3 in node_tags_element.reshape(element_tags.shape[0], 3):
            for pa in [p1, p2, p3]:
                for pb in [p1, p2, p3]:
                    corner_x = a + (pa - 1) * w
                    corner_y = 2.5 - (pb - 1) * w
                    f.write(f"\\coordinate (c) at ({corner_x:.4f}, {corner_y:.4f});\n")
                    f.write(f'\\fill [fill=mygreen, fill opacity=0.1] (c) -- ($(c) + (0, -\w)$) -- ($(c) + (\w, -\w)$) -- ($(c) + (\w, 0)$) -- (c);\n')

                corner_y = 2.5 - (pa - 1) * w - 0.05 * w
                f.write(f"\\coordinate (c) at (2.5, {corner_y:.4f});\n")
                f.write(f'\\fill [fill=myred, fill opacity=0.1] (c) -- ($(c) + (0, -0.9*\w)$) -- ($(c) + (1, -0.9*\w)$) -- ($(c) + (1, 0)$) -- (c);\n')



gmsh.initialize()

for lc, name in [
        # (30, "coarse"),
        # (30, "medium"),
        # (4, "fine"),
        (4, "coarse"),
        (1.5, "medium"),
        (0.8, "fine"),
]:
    write_mesh(lc, name)  


gmsh.finalize()


